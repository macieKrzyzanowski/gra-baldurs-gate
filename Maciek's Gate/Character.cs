﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Klasa abstrakcuje przechowujaca wszystkie wlasciwosci i metody, ktore zawiera kazda z postaci, ktora gra gracz.
    /// Użycie klasy akstrakcyjnej zapobiega konieczności przepisywania kodu, przy deklaracji kazdej z klas postaci
    /// Klasa ta ma zimplementowany interface, opisany w pliku ICharacterProperties.cs
    /// </summary>
    abstract public class Character : ICharacterProperties
    {
        /// <summary>
        /// Nazwa specjalnosci postaci.
        /// </summary>
        public string Specialization { get; set; }
        /// <summary>
        /// Obecne zycie postaci
        /// </summary>
        public int Health { get; set; }
        /// <summary>
        /// Maksymalne zycie postaci.
        /// </summary>
        public int MaxHealth { get; set; }
        /// <summary>
        /// Zmienna przechowuje informacje czy postac jest zywa, czy martwa
        /// false - jest zywa
        /// true - jest martwa
        /// </summary>
        public bool HeIsDead { get; set; }
        /// <summary>
        /// Level postaci
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Minimalny atak, jaki moze wyprowadzic postac
        /// </summary>
        public int MinimalAttack { get; set; }
        /// <summary>
        /// Maksymalny atak, jaki moze wyprowadzic postac
        /// </summary>
        public int MaximumAttack { get; set; }
        /// <summary>
        /// Szanse postaci na unik (w %)
        /// </summary>
        public int ChanceDefense { get; set; }
        /// <summary>
        /// Rodzaj akcji, jaka wykona postac w kolejnej turze
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// Konstruktor bezparametrowy, ktory ustala standardowe parametry kazdej nowej postaci
        /// </summary>
        protected Character()
        {
            Level = 1;
            HeIsDead = false;
            Action = 0;
        }


        /// <summary>
        /// Metoda odpowiada za wyprowadzanie ataku.
        /// </summary>
        /// <returns>wartosc wyprowadzonego ataku</returns>
        public virtual int Attack()
        {
            //losujemy liczbe z przedzialu MinimalAttack - MaximumAttack
            Random random = new Random();
            return random.Next(MinimalAttack, MaximumAttack);
        }

        /// <summary>
        /// Metoda odpowiada z przyjmowanie ataku z szansą obrony
        /// </summary>
        /// <param name="attackValue">Wartośc otrzymywanego aktaku</param>
        public void Defend(int attackValue)
        {
            //losujemy liczne od 0 do 100
            Random random = new Random();
            int result = random.Next(0, 100);

            //jezeli wylosowana liczba jest wieksza niz szansa na obrone, postac otrzymuje obrazenia
            //odpowiada to szansie procentowej na unik
            if (result > ChanceDefense)
            {
                Health -= attackValue;
                //jezeli zycie spadnie do 0 lub mniej, postac umiera
                if (Health <= 0)
                    HeIsDead = true;
            }

        }

        /// <summary>
        /// Metoda odpowiadajaca za zwiekszenie poziomu postaci oraz zwiekszenie jej statystyk
        /// </summary>
        public virtual void LevelUp()
        {
            Level++;
            MaxHealth = MaxHealth + 10 * Level;
            Health = MaxHealth;
            MinimalAttack *= 2;
            MaximumAttack *= 2;
            ChanceDefense++;
        }

        /// <summary>
        /// Metoda odpowiada za przyjecie akatu, bez szansy obrony
        /// </summary>
        /// <param name="attackValue">Wartośc otrzymywanego aktaku</param>
        public void TakeDemage(int attackValue)
        {
            Health -= attackValue;
            //jezeli zycie spadnie do 0 lub mniej, postac umiera
            if (Health <= 0)
                HeIsDead = true;
        }

        /// <summary>
        /// Metoda zmienia wartość zmiennej przechiwujacej info. o rodzaju akcji, jaka wykona postac w kolejnej turze
        /// </summary>
        /// <param name="action">
        /// 1 - atak
        /// 2 - obrona
        /// (opcjonalnie dla Maga)
        /// 3 - leczenie
        /// </param>
        public virtual void SetAction(int action)
        {
            Action = action;
        }

        /// <summary>
        /// Metoda pobiera informacje na temat akcji, jaka wykona postać
        /// </summary>
        /// <returns>
        /// Rodzaj akcji jaka wykona postac
        /// 1 - atak
        /// 2 - obrona
        /// (opcjonalnie dla Maga)
        /// 3 - leczenie
        /// </returns>
        public virtual int GetAction()
        {
            return Action;
        }

        /// <summary>
        /// Metoda odpowiada za leczenie postaci podanej w parametrze funckji (metoda zimplementowana w klasie Maga)
        /// </summary>
        /// <param name="healCharacter">Leczona postac</param>
        public virtual void Heal(Character healCharacter) {}
    }
}
