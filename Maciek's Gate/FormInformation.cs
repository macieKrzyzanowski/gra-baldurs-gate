﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaciejKrzyżanowskiLab2Zad1
{
    public partial class FormInformation : Form
    {
        /// <summary>
        /// wywolanie okna, informujacego gracza o zwyciestwie lub porażce
        /// </summary>
        /// <param name="counter">
        /// 1 - zwyciestwo
        /// 2 - porażka
        /// </param>
        public FormInformation(int counter)
        {
            InitializeComponent();
            //jezeli zwyciestwo
            if(counter == 1)
            {
                labelInformation.Text = "ZWYCIESTWO";
                labelInformation.ForeColor = Color.Green;
                buttonInformation.Text = "Kolejna tura!";
            }
            //jezeli porazka
            else if(counter == 2)
            {
                labelInformation.Text = "PORAZKA";
                labelInformation.ForeColor = Color.Red;
                buttonInformation.Text = "Zakończ";
            }
           
        }

        /// <summary>
        /// Przycisk sluzy do akceptacji powiadomienia okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInformation_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
