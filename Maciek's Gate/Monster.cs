﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Klasa potwora, z jakim walcza postacie w grze. 
    /// Klasa ta ma zaimplementowany interface, z podstawowymi parametrami i metodami jakie powinna zawiarac kazda postac w grze
    /// </summary>
    public class Monster : ICharacterProperties
    {
        /// <summary>
        /// Nazwa specjalnosci postaci.
        /// </summary>
        public string Specialization { get; set; }
        /// <summary>
        /// Obecne zycie postaci
        /// </summary>
        public int Health { get; set; }
        /// <summary>
        /// Maksymalne zycie postaci.
        /// </summary>
        public int MaxHealth { get; set; }
        /// <summary>
        /// Zmienna przechowuje informacje czy postac jest zywa, czy martwa
        /// false - jest zywa
        /// true - jest martwa
        /// </summary>
        public bool HeIsDead { get; set; }
        /// <summary>
        /// Level postaci
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Minimalny atak, jaki moze wyprowadzic postac
        /// </summary>
        public int MinimalAttack { get; set; }
        /// <summary>
        /// Maksymalny atak, jaki moze wyprowadzic postac
        /// </summary>
        public int MaximumAttack { get; set; }
        /// <summary>
        /// Szanse postaci na unik (w %)
        /// </summary>
        public int ChanceDefense { get; set; }


        /// <summary>
        /// konstruktor z parametrem okreslajacym poziom postaci.
        /// Okrezla on poczatkowe wartosci postaci
        /// </summary>
        /// <param name="newLevel">Level postaci</param>
        public Monster(int newLevel)
        {
            Specialization = "Goblin";
            Level = newLevel;
            MaxHealth = Health = Level*350;
            HeIsDead = false;
            MinimalAttack = Level * 6;
            MaximumAttack = Level * 9;
            ChanceDefense = Level;
        }


        /// <summary>
        /// Metoda odpowiada za wyprowadzanie ataku.
        /// </summary>
        /// <returns>wartosc wyprowadzonego ataku</returns>
        public int Attack()
        {
            Random random = new Random();
            return random.Next(MinimalAttack, MaximumAttack);
        }

        /// <summary>
        /// Metoda odpowiada z przyjmowanie ataku z szansą obrony
        /// </summary>
        /// <param name="attackValue">Wartośc otrzymywanego aktaku</param>
        public void Defend(int attackValue)
        {
            Random random = new Random();
            int result = random.Next(0, 100);

            if (result > ChanceDefense)
            {
                Health -= attackValue;
                if (Health < 0)
                    HeIsDead = true;
            }
                
        }

        /// <summary>
        /// Metoda odpowiadajaca za zwiekszenie poziomu postaci oraz zwiekszenie jej statystyk
        /// </summary>
        public void LevelUp()
        {
            Level++;
            MaxHealth = MaxHealth + 100 * Level;
            Health = MaxHealth;
            MinimalAttack *= 3;
            MaximumAttack *= 4;
            ChanceDefense++;
            HeIsDead = false;
        }

        /// <summary>
        /// Metoda odpowiada za wylosowanie postaci, w ktora zaostanie wymierzony atak w kolejnej turze
        /// </summary>
        /// <param name="randomTo">Ilosc postaci z ktorymi walczy potwor</param>
        /// <returns>Wylosowana postac, w ktora zostanie wymierzony atak</returns>
        public int ChoiceTarget(int randomTo)
        {
            Random random = new Random();
            return random.Next(0, randomTo);
        }

    }
}
