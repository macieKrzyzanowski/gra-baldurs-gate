﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace MaciejKrzyżanowskiLab2Zad1
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Potor na poziomie 1 z ktora walcza bohaterzy
        /// </summary>
        Monster monster = new Monster(1); 
        /// <summary>
        /// Tablica przechowuje bohaterow
        /// 0 - warriow
        /// 1 - rouge
        /// 2 - mage
        /// </summary>
        Character[] character = new Character[3]
        {
            new Warrior(),
            new Rouge(),
            new Mage()
        };

        /// <summary>
        /// Tablica przechowuje napisy o statystyka bohaterow
        /// 0 - stat. warrior
        /// 1 - stat. rouge
        /// 2 - stat. mage
        /// </summary>
        Label[] labelInformation;


        public FormMain()
        {
            InitializeComponent();
            //inicjalizacja tablicy o statystykach bohaterow
            labelInformation = new Label[]
            {
                labelWarriorInformation,
                labelRougeInformation,
                labelMageInformation,
           };
            //aktualizacja informacji w grze
            updateInformation();
        }

        /// <summary>
        /// Zmienna przechouje informacje na temat numery aktualnej tury
        /// </summary>
        int turnNumber = 1;

        /// <summary>
        /// Metoda aktualiuje informacje w grze
        /// </summary>
        void updateInformation()
        {
            //aktualizacja informacji o nr tury
            labelTurnNumber.Text = "Tura nr: " + turnNumber;
            //aktualizacja informacji o bohaterach
            for (int i = 0; i < labelInformation.Length; i++)
            {
                labelInformation[i].Text = "Lvl.: " + character[i].Level + Environment.NewLine +
                                           "Health: " + character[i].Health + "\\" + character[i].MaxHealth + Environment.NewLine +
                                           "Attack: (" + character[i].MinimalAttack + "-" + character[i].MaximumAttack + ")" + Environment.NewLine +
                                           "Chance to defense:" + character[i].ChanceDefense + "%";
            }

            //aktualizacja informacji o przeciwniku
            labelMonsterInformation.Text = "Lvl.: " + monster.Level + Environment.NewLine +
                                           "Health: " + monster.Health + "\\" + monster.MaxHealth + Environment.NewLine +
                                           "Attack: (" + monster.MinimalAttack + "-" + monster.MaximumAttack + ")" + Environment.NewLine +
                                           "Chance to defense:" + monster.ChanceDefense + "%";
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci warrior na atak
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonWarriorAttack_Click(object sender, EventArgs e)
        {
            character[0].SetAction(1);
            buttonWarriorAttack.BackColor = Color.Red;
            buttonWarriorDefend.BackColor = Color.LightCoral;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci warrior na obrone
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonWarriorDefend_Click(object sender, EventArgs e)
        {
            character[0].SetAction(2);
            buttonWarriorAttack.BackColor = Color.LightCoral;
            buttonWarriorDefend.BackColor = Color.Red;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci rouge na atak
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRougeAttack_Click(object sender, EventArgs e)
        {
            character[1].SetAction(1);
            buttonRougeAttack.BackColor = Color.Green;
            buttonRougeDefend.BackColor = Color.LightGreen;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci rouge na obrone
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRougeDefend_Click(object sender, EventArgs e)
        {
            character[1].SetAction(2);
            buttonRougeAttack.BackColor = Color.LightGreen;
            buttonRougeDefend.BackColor = Color.Green;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci maga na atak
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMageAttack_Click(object sender, EventArgs e)
        {
            character[2].SetAction(1);
            buttonMageAttack.BackColor = Color.Blue;
            buttonMageDefend.BackColor = Color.LightBlue;
            buttonMageHeal.BackColor = Color.LightBlue;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci maga na obrone
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMageDefend_Click(object sender, EventArgs e)
        {
            character[2].SetAction(2);
            buttonMageAttack.BackColor = Color.LightBlue;
            buttonMageDefend.BackColor = Color.Blue;
            buttonMageHeal.BackColor = Color.LightBlue;
        }

        /// <summary>
        /// Przycisk zmienia kolejna akcje postaci maga na leczenie postaci
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMageHeal_Click(object sender, EventArgs e)
        {
            character[2].SetAction(3);
            buttonMageAttack.BackColor = Color.LightBlue;
            buttonMageDefend.BackColor = Color.LightBlue;
            buttonMageHeal.BackColor = Color.Blue;
        }

        /// <summary>
        /// Funkcja odpowiada za wykonanie tury
        /// </summary>
        /// <returns>
        /// true - jezeli udalo sie wykonac ture
        /// false - jezeli nie udalo sie wykonac tury
        /// </returns>
        bool makeTurn()
        {
            //sprawdzenie czy akcje wszystkich bohaterow zostaly wybrane
            for (int i = 0; i < character.Length; i++)
            {
                if (character[i].GetAction() == 0)
                {
                    MessageBox.Show("Wybierz jaką akcje ma wykonać " + character[i].Specialization + "!");
                    return false;
                }
            }

            //jezeli mag chce uzdrowic 
            if (character[2].GetAction() == 3)
            {
                //powinien miec zaznaczone jaka postac chce uzdrowic za pomoca radioButton
                //jezeli jakij radioButton jest zaznaczony, Mag leczy odpowiadnia postac
                if (radioButtonHealWarrior.Checked == true)
                    character[2].Heal(character[0]);
                else if (radioButtonHealRouge.Checked == true)
                    character[2].Heal(character[1]);
                else if (radioButtonHealMage.Checked == true)
                    character[2].Heal(character[2]);
                //jezeli zadana postac nie jest zaznaczona
                else
                {
                    MessageBox.Show("Zaznacz kogo ma uleczyć mag!");
                    return false;
                }
            }

            //sprawdzanie ktore postacie maja zazaczona opcje ataku
            for (int i = 0; i < character.Length; i++)
            {
                //jezeli postac ma zaznaczona opcje ataku, zostanie wykonany atak na potworze, ktory bedzie sie bronic
                if (character[0].GetAction() == 1)
                    monster.Defend(character[0].Attack());
            }

            //pobieranie postaci, ktora potworz "postanowil" zaatakowac
            int choice = monster.ChoiceTarget(character.Length);

            //jezeli wybrana postac za zaznacona opcje obrony, postac ma wyolana metode, ktora daje szonse obrony
            if (character[choice].GetAction() == 2)
                character[choice].Defend(monster.Attack());
            //jezeli nie ma zaznaczonej opcji obrony, postac otrzymuje obrazenia, bez mozliwosci obrony
            else
                character[choice].TakeDemage(monster.Attack());

            //zwikszamy numer tury
            turnNumber++;

            return true;

        }

        /// <summary>
        /// Przycisk odpowiadajacy za wykonanie tury
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEndTurn_Click(object sender, EventArgs e)
        {
            //jezeli wykonanie tury powiodlo sie
            if (makeTurn() == true)
            {
                //sprawdzamy czy ktoras z postaci nie zginela
                if (character[0].HeIsDead || character[1].HeIsDead || character[2].HeIsDead)
                {
                    //jezli ktoras z postaci zginela, konczymy gre
                    FormInformation formInformation = new FormInformation(2);
                    formInformation.ShowDialog();
                    Close();
                }
                //jezeli zgina potwor
                else if (monster.HeIsDead) 
                {
                    //informujemy gracza o zwyciestwie
                    FormInformation formInformation = new FormInformation(1);
                    formInformation.ShowDialog();

                    //zwiekszamy poziomy postaci
                    monster.LevelUp();
                    character[0].LevelUp();
                    character[1].LevelUp();
                    character[2].LevelUp();

                    //rozpoczynamy nowa ture
                    turnNumber = 1;
                }

                //aktualizujemy informacje w grze
                updateInformation();

            }
        }


    }
}
