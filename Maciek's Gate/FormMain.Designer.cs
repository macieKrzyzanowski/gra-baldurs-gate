﻿namespace MaciejKrzyżanowskiLab2Zad1
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.pictureBoxWarrior = new System.Windows.Forms.PictureBox();
            this.pictureBoxRouge = new System.Windows.Forms.PictureBox();
            this.pictureBoxMage = new System.Windows.Forms.PictureBox();
            this.pictureBoxMonster = new System.Windows.Forms.PictureBox();
            this.buttonWarriorAttack = new System.Windows.Forms.Button();
            this.buttonWarriorDefend = new System.Windows.Forms.Button();
            this.labelTurnNumber = new System.Windows.Forms.Label();
            this.buttonEndTurn = new System.Windows.Forms.Button();
            this.labelWarriorInformation = new System.Windows.Forms.Label();
            this.labelMonsterInformation = new System.Windows.Forms.Label();
            this.labelRougeInformation = new System.Windows.Forms.Label();
            this.labelMageInformation = new System.Windows.Forms.Label();
            this.buttonRougeAttack = new System.Windows.Forms.Button();
            this.buttonRougeDefend = new System.Windows.Forms.Button();
            this.buttonMageAttack = new System.Windows.Forms.Button();
            this.buttonMageDefend = new System.Windows.Forms.Button();
            this.labelWarriorName = new System.Windows.Forms.Label();
            this.labelRougeName = new System.Windows.Forms.Label();
            this.labelMageName = new System.Windows.Forms.Label();
            this.labelNameMonster = new System.Windows.Forms.Label();
            this.buttonMageHeal = new System.Windows.Forms.Button();
            this.radioButtonHealWarrior = new System.Windows.Forms.RadioButton();
            this.radioButtonHealRouge = new System.Windows.Forms.RadioButton();
            this.radioButtonHealMage = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarrior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRouge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMonster)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxWarrior
            // 
            this.pictureBoxWarrior.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWarrior.Image")));
            this.pictureBoxWarrior.Location = new System.Drawing.Point(80, 143);
            this.pictureBoxWarrior.Name = "pictureBoxWarrior";
            this.pictureBoxWarrior.Size = new System.Drawing.Size(115, 183);
            this.pictureBoxWarrior.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWarrior.TabIndex = 0;
            this.pictureBoxWarrior.TabStop = false;
            // 
            // pictureBoxRouge
            // 
            this.pictureBoxRouge.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRouge.Image")));
            this.pictureBoxRouge.Location = new System.Drawing.Point(343, 143);
            this.pictureBoxRouge.Name = "pictureBoxRouge";
            this.pictureBoxRouge.Size = new System.Drawing.Size(115, 183);
            this.pictureBoxRouge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRouge.TabIndex = 1;
            this.pictureBoxRouge.TabStop = false;
            // 
            // pictureBoxMage
            // 
            this.pictureBoxMage.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMage.Image")));
            this.pictureBoxMage.Location = new System.Drawing.Point(591, 143);
            this.pictureBoxMage.Name = "pictureBoxMage";
            this.pictureBoxMage.Size = new System.Drawing.Size(115, 183);
            this.pictureBoxMage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMage.TabIndex = 2;
            this.pictureBoxMage.TabStop = false;
            // 
            // pictureBoxMonster
            // 
            this.pictureBoxMonster.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMonster.Image")));
            this.pictureBoxMonster.Location = new System.Drawing.Point(343, 446);
            this.pictureBoxMonster.Name = "pictureBoxMonster";
            this.pictureBoxMonster.Size = new System.Drawing.Size(115, 183);
            this.pictureBoxMonster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMonster.TabIndex = 3;
            this.pictureBoxMonster.TabStop = false;
            // 
            // buttonWarriorAttack
            // 
            this.buttonWarriorAttack.BackColor = System.Drawing.Color.LightCoral;
            this.buttonWarriorAttack.Location = new System.Drawing.Point(80, 332);
            this.buttonWarriorAttack.Name = "buttonWarriorAttack";
            this.buttonWarriorAttack.Size = new System.Drawing.Size(115, 23);
            this.buttonWarriorAttack.TabIndex = 4;
            this.buttonWarriorAttack.Text = "Atakuj !";
            this.buttonWarriorAttack.UseVisualStyleBackColor = false;
            this.buttonWarriorAttack.Click += new System.EventHandler(this.buttonWarriorAttack_Click);
            // 
            // buttonWarriorDefend
            // 
            this.buttonWarriorDefend.BackColor = System.Drawing.Color.LightCoral;
            this.buttonWarriorDefend.Location = new System.Drawing.Point(80, 361);
            this.buttonWarriorDefend.Name = "buttonWarriorDefend";
            this.buttonWarriorDefend.Size = new System.Drawing.Size(115, 23);
            this.buttonWarriorDefend.TabIndex = 5;
            this.buttonWarriorDefend.Text = "Broń się";
            this.buttonWarriorDefend.UseVisualStyleBackColor = false;
            this.buttonWarriorDefend.Click += new System.EventHandler(this.buttonWarriorDefend_Click);
            // 
            // labelTurnNumber
            // 
            this.labelTurnNumber.AutoSize = true;
            this.labelTurnNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTurnNumber.Location = new System.Drawing.Point(312, 0);
            this.labelTurnNumber.Name = "labelTurnNumber";
            this.labelTurnNumber.Size = new System.Drawing.Size(47, 39);
            this.labelTurnNumber.TabIndex = 6;
            this.labelTurnNumber.Text = "...";
            // 
            // buttonEndTurn
            // 
            this.buttonEndTurn.Location = new System.Drawing.Point(343, 60);
            this.buttonEndTurn.Name = "buttonEndTurn";
            this.buttonEndTurn.Size = new System.Drawing.Size(115, 23);
            this.buttonEndTurn.TabIndex = 7;
            this.buttonEndTurn.Text = "Zakończ ture!";
            this.buttonEndTurn.UseVisualStyleBackColor = true;
            this.buttonEndTurn.Click += new System.EventHandler(this.buttonEndTurn_Click);
            // 
            // labelWarriorInformation
            // 
            this.labelWarriorInformation.AutoSize = true;
            this.labelWarriorInformation.Location = new System.Drawing.Point(201, 261);
            this.labelWarriorInformation.Name = "labelWarriorInformation";
            this.labelWarriorInformation.Size = new System.Drawing.Size(16, 13);
            this.labelWarriorInformation.TabIndex = 8;
            this.labelWarriorInformation.Text = "...";
            // 
            // labelMonsterInformation
            // 
            this.labelMonsterInformation.AutoSize = true;
            this.labelMonsterInformation.Location = new System.Drawing.Point(340, 632);
            this.labelMonsterInformation.Name = "labelMonsterInformation";
            this.labelMonsterInformation.Size = new System.Drawing.Size(16, 13);
            this.labelMonsterInformation.TabIndex = 9;
            this.labelMonsterInformation.Text = "...";
            // 
            // labelRougeInformation
            // 
            this.labelRougeInformation.AutoSize = true;
            this.labelRougeInformation.Location = new System.Drawing.Point(464, 261);
            this.labelRougeInformation.Name = "labelRougeInformation";
            this.labelRougeInformation.Size = new System.Drawing.Size(16, 13);
            this.labelRougeInformation.TabIndex = 10;
            this.labelRougeInformation.Text = "...";
            // 
            // labelMageInformation
            // 
            this.labelMageInformation.AutoSize = true;
            this.labelMageInformation.Location = new System.Drawing.Point(712, 261);
            this.labelMageInformation.Name = "labelMageInformation";
            this.labelMageInformation.Size = new System.Drawing.Size(16, 13);
            this.labelMageInformation.TabIndex = 11;
            this.labelMageInformation.Text = "...";
            // 
            // buttonRougeAttack
            // 
            this.buttonRougeAttack.BackColor = System.Drawing.Color.LightGreen;
            this.buttonRougeAttack.Location = new System.Drawing.Point(343, 332);
            this.buttonRougeAttack.Name = "buttonRougeAttack";
            this.buttonRougeAttack.Size = new System.Drawing.Size(115, 23);
            this.buttonRougeAttack.TabIndex = 12;
            this.buttonRougeAttack.Text = "Atakuj !";
            this.buttonRougeAttack.UseVisualStyleBackColor = false;
            this.buttonRougeAttack.Click += new System.EventHandler(this.buttonRougeAttack_Click);
            // 
            // buttonRougeDefend
            // 
            this.buttonRougeDefend.BackColor = System.Drawing.Color.LightGreen;
            this.buttonRougeDefend.Location = new System.Drawing.Point(343, 361);
            this.buttonRougeDefend.Name = "buttonRougeDefend";
            this.buttonRougeDefend.Size = new System.Drawing.Size(115, 23);
            this.buttonRougeDefend.TabIndex = 13;
            this.buttonRougeDefend.Text = "Broń się";
            this.buttonRougeDefend.UseVisualStyleBackColor = false;
            this.buttonRougeDefend.Click += new System.EventHandler(this.buttonRougeDefend_Click);
            // 
            // buttonMageAttack
            // 
            this.buttonMageAttack.BackColor = System.Drawing.Color.LightBlue;
            this.buttonMageAttack.Location = new System.Drawing.Point(591, 332);
            this.buttonMageAttack.Name = "buttonMageAttack";
            this.buttonMageAttack.Size = new System.Drawing.Size(115, 23);
            this.buttonMageAttack.TabIndex = 14;
            this.buttonMageAttack.Text = "Atakuj !";
            this.buttonMageAttack.UseVisualStyleBackColor = false;
            this.buttonMageAttack.Click += new System.EventHandler(this.buttonMageAttack_Click);
            // 
            // buttonMageDefend
            // 
            this.buttonMageDefend.BackColor = System.Drawing.Color.LightBlue;
            this.buttonMageDefend.Location = new System.Drawing.Point(591, 361);
            this.buttonMageDefend.Name = "buttonMageDefend";
            this.buttonMageDefend.Size = new System.Drawing.Size(115, 23);
            this.buttonMageDefend.TabIndex = 15;
            this.buttonMageDefend.Text = "Broń się";
            this.buttonMageDefend.UseVisualStyleBackColor = false;
            this.buttonMageDefend.Click += new System.EventHandler(this.buttonMageDefend_Click);
            // 
            // labelWarriorName
            // 
            this.labelWarriorName.AutoSize = true;
            this.labelWarriorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWarriorName.ForeColor = System.Drawing.Color.DarkRed;
            this.labelWarriorName.Location = new System.Drawing.Point(88, 120);
            this.labelWarriorName.Name = "labelWarriorName";
            this.labelWarriorName.Size = new System.Drawing.Size(95, 20);
            this.labelWarriorName.TabIndex = 16;
            this.labelWarriorName.Text = "WARRIOR";
            // 
            // labelRougeName
            // 
            this.labelRougeName.AutoSize = true;
            this.labelRougeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRougeName.ForeColor = System.Drawing.Color.DarkGreen;
            this.labelRougeName.Location = new System.Drawing.Point(363, 120);
            this.labelRougeName.Name = "labelRougeName";
            this.labelRougeName.Size = new System.Drawing.Size(74, 20);
            this.labelRougeName.TabIndex = 17;
            this.labelRougeName.Text = "ROUGE";
            // 
            // labelMageName
            // 
            this.labelMageName.AutoSize = true;
            this.labelMageName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMageName.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelMageName.Location = new System.Drawing.Point(619, 120);
            this.labelMageName.Name = "labelMageName";
            this.labelMageName.Size = new System.Drawing.Size(61, 20);
            this.labelMageName.TabIndex = 18;
            this.labelMageName.Text = "MAGE";
            // 
            // labelNameMonster
            // 
            this.labelNameMonster.AutoSize = true;
            this.labelNameMonster.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNameMonster.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelNameMonster.Location = new System.Drawing.Point(363, 423);
            this.labelNameMonster.Name = "labelNameMonster";
            this.labelNameMonster.Size = new System.Drawing.Size(76, 20);
            this.labelNameMonster.TabIndex = 19;
            this.labelNameMonster.Text = "GOBLIN";
            // 
            // buttonMageHeal
            // 
            this.buttonMageHeal.BackColor = System.Drawing.Color.LightBlue;
            this.buttonMageHeal.Location = new System.Drawing.Point(591, 390);
            this.buttonMageHeal.Name = "buttonMageHeal";
            this.buttonMageHeal.Size = new System.Drawing.Size(115, 23);
            this.buttonMageHeal.TabIndex = 20;
            this.buttonMageHeal.Text = "Ulecz:";
            this.buttonMageHeal.UseVisualStyleBackColor = false;
            this.buttonMageHeal.Click += new System.EventHandler(this.buttonMageHeal_Click);
            // 
            // radioButtonHealWarrior
            // 
            this.radioButtonHealWarrior.AutoSize = true;
            this.radioButtonHealWarrior.Location = new System.Drawing.Point(715, 395);
            this.radioButtonHealWarrior.Name = "radioButtonHealWarrior";
            this.radioButtonHealWarrior.Size = new System.Drawing.Size(59, 17);
            this.radioButtonHealWarrior.TabIndex = 21;
            this.radioButtonHealWarrior.TabStop = true;
            this.radioButtonHealWarrior.Text = "Warrior";
            this.radioButtonHealWarrior.UseVisualStyleBackColor = true;
            // 
            // radioButtonHealRouge
            // 
            this.radioButtonHealRouge.AutoSize = true;
            this.radioButtonHealRouge.Location = new System.Drawing.Point(715, 418);
            this.radioButtonHealRouge.Name = "radioButtonHealRouge";
            this.radioButtonHealRouge.Size = new System.Drawing.Size(57, 17);
            this.radioButtonHealRouge.TabIndex = 22;
            this.radioButtonHealRouge.TabStop = true;
            this.radioButtonHealRouge.Text = "Rouge";
            this.radioButtonHealRouge.UseVisualStyleBackColor = true;
            // 
            // radioButtonHealMage
            // 
            this.radioButtonHealMage.AutoSize = true;
            this.radioButtonHealMage.Location = new System.Drawing.Point(715, 441);
            this.radioButtonHealMage.Name = "radioButtonHealMage";
            this.radioButtonHealMage.Size = new System.Drawing.Size(52, 17);
            this.radioButtonHealMage.TabIndex = 23;
            this.radioButtonHealMage.TabStop = true;
            this.radioButtonHealMage.Text = "Mage";
            this.radioButtonHealMage.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(829, 717);
            this.Controls.Add(this.radioButtonHealMage);
            this.Controls.Add(this.radioButtonHealRouge);
            this.Controls.Add(this.radioButtonHealWarrior);
            this.Controls.Add(this.buttonMageHeal);
            this.Controls.Add(this.labelNameMonster);
            this.Controls.Add(this.labelMageName);
            this.Controls.Add(this.labelRougeName);
            this.Controls.Add(this.labelWarriorName);
            this.Controls.Add(this.buttonMageDefend);
            this.Controls.Add(this.buttonMageAttack);
            this.Controls.Add(this.buttonRougeDefend);
            this.Controls.Add(this.buttonRougeAttack);
            this.Controls.Add(this.labelMageInformation);
            this.Controls.Add(this.labelRougeInformation);
            this.Controls.Add(this.labelMonsterInformation);
            this.Controls.Add(this.labelWarriorInformation);
            this.Controls.Add(this.buttonEndTurn);
            this.Controls.Add(this.labelTurnNumber);
            this.Controls.Add(this.buttonWarriorDefend);
            this.Controls.Add(this.buttonWarriorAttack);
            this.Controls.Add(this.pictureBoxMonster);
            this.Controls.Add(this.pictureBoxMage);
            this.Controls.Add(this.pictureBoxRouge);
            this.Controls.Add(this.pictureBoxWarrior);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maciek\'s Gate";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarrior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRouge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMonster)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxWarrior;
        private System.Windows.Forms.PictureBox pictureBoxRouge;
        private System.Windows.Forms.PictureBox pictureBoxMage;
        private System.Windows.Forms.PictureBox pictureBoxMonster;
        private System.Windows.Forms.Button buttonWarriorAttack;
        private System.Windows.Forms.Button buttonWarriorDefend;
        private System.Windows.Forms.Label labelTurnNumber;
        private System.Windows.Forms.Button buttonEndTurn;
        private System.Windows.Forms.Label labelWarriorInformation;
        private System.Windows.Forms.Label labelMonsterInformation;
        private System.Windows.Forms.Label labelRougeInformation;
        private System.Windows.Forms.Label labelMageInformation;
        private System.Windows.Forms.Button buttonRougeAttack;
        private System.Windows.Forms.Button buttonRougeDefend;
        private System.Windows.Forms.Button buttonMageAttack;
        private System.Windows.Forms.Button buttonMageDefend;
        private System.Windows.Forms.Label labelWarriorName;
        private System.Windows.Forms.Label labelRougeName;
        private System.Windows.Forms.Label labelMageName;
        private System.Windows.Forms.Label labelNameMonster;
        private System.Windows.Forms.Button buttonMageHeal;
        private System.Windows.Forms.RadioButton radioButtonHealWarrior;
        private System.Windows.Forms.RadioButton radioButtonHealRouge;
        private System.Windows.Forms.RadioButton radioButtonHealMage;
    }
}

