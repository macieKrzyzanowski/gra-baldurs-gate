﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Klasa postaci Warrior, dziedzicząca parametry i metody po klasie abstrakcyjnej Character
    /// </summary>
    class Mage : Character
    {
        /// <summary>
        /// Minimalna wartosc z jaka mag moze uzdrowic postac
        /// </summary>
        int minimalHeal { get; set; }
        /// <summary>
        /// Maksymalna wartosc z jaka mag moze uzdrowic postac
        /// </summary>
        int maximumHeal { get; set; }

        /// <summary>
        /// Konstruktor wywolujacy konstuktor klasu akstrakcyjnej, ustawiajacej podstawowe parametyr postaci
        /// oraz ustawiajacy parametry charakterystyczne dla tej klasy postaci
        /// </summary>
        public Mage() : base()
        {
            Specialization = "Sorcerer";
            MaxHealth = Health = 100;
            MinimalAttack = 10;
            MaximumAttack = 15;
            ChanceDefense = 7;
            minimalHeal = 7;
            maximumHeal = 10;
        }

        /// <summary>
        /// Metoda zwieksza poziom postaci. 
        /// Wywoluje ona metoda z klasy abstrakcyjej oraz zwieksza parametry postaci,
        /// charakterystyczne tylko dla tej klasy
        /// </summary>
        public override void LevelUp()
        {
            base.LevelUp();
            minimalHeal = minimalHeal * Level;
            maximumHeal = maximumHeal * Level;
        }

        /// <summary>
        /// Metoda odpowiada za leczenie postaci podanej w parametrze funckji
        /// </summary>
        /// <param name="healCharacter">Leczona postac</param>
        public override void Heal(Character healCharacter)
        {
            //wylosowanie wartosci leczenia 
            Random random = new Random();
            int healthCharacter = healCharacter.Health; //poziom zdrowia postaci
            int maxHealthCharacter = healCharacter.MaxHealth; //masymalny poziom zdrowia postaci 
            int randomHeal = random.Next(minimalHeal, maximumHeal);

            //jezeli wylosowana wartosc zdrowia i obecna wartosc zdrowia jest menijsza niz masymalna wartosc jaka moze miec dana postac
            if (healthCharacter + randomHeal < maxHealthCharacter)
                healCharacter.Health = healthCharacter + randomHeal;
            //w przeciwnym wypadku wylecz do maksymalnej mozliwej wartosci zdrowia  
            else
                healCharacter.Health = maxHealthCharacter;
        }
    }
}
