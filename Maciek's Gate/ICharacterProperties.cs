﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Interfejs zawiera w sobie wszystkie właściwości jakie powinny zawierać w sobie wszystkie postacie występujące w grze
    /// </summary>
    interface ICharacterProperties
    {
        /// <summary>
        /// Nazwa specjalnosci postaci.
        /// </summary>
        string Specialization { get; set; }
        /// <summary>
        /// Obecne zycie postaci
        /// </summary>
        int Health { get; set; }
        /// <summary>
        /// Maksymalne zycie postaci.
        /// </summary>
        int MaxHealth { get; set; }
        /// <summary>
        /// Zmienna przechowuje informacje czy postac jest zywa, czy martwa
        /// false - jest zywa
        /// true - jest martwa
        /// </summary>
        bool HeIsDead { get; set; }
        /// <summary>
        /// Level postaci
        /// </summary>
        int Level { get; set; }
        /// <summary>
        /// Minimalny atak, jaki moze wyprowadzic postac
        /// </summary>
        int MinimalAttack { get; set; }
        /// <summary>
        /// Maksymalny atak, jaki moze wyprowadzic postac
        /// </summary>
        int MaximumAttack { get; set; }
        /// <summary>
        /// Szanse postaci na unik 
        /// </summary>
        int ChanceDefense { get; set; }

        /// <summary>
        /// Metoda odpowiada za wyprowadzania ataku
        /// </summary>
        /// <returns>Wartość ataku</returns>
        int Attack();
        /// <summary>
        /// Metoda odpowiada za przyjmowanie ataku
        /// </summary>
        /// <param name="attackValue">Wartość ataku przeciwnika</param>
        void Defend(int attackValue);
        /// <summary>
        /// Metoda odpowiada za zwiększenie poziomu postaci
        /// </summary>
        void LevelUp();
        
    }
}
