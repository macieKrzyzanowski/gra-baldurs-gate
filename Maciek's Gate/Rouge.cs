﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Klasa postaci Rouge, dziedzicząca parametry i metody po klasie abstrakcyjnej Character
    /// </summary>
    public class Rouge : Character
    {
        /// <summary>
        /// Konstruktor wywolujacy konstuktor klasu akstrakcyjnej, ustawiajacej podstawowe parametyr postaci
        /// oraz ustawiajacy parametry charakterystyczne dla tej klasy postaci
        /// </summary>
        public Rouge() : base()
        {
            Specialization = "Rouge";
            MaxHealth = Health = 150;
            MinimalAttack = 2;
            MaximumAttack = 20;
            ChanceDefense = 10;
        }
    }
}
