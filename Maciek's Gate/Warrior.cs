﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaciejKrzyżanowskiLab2Zad1
{
    /// <summary>
    /// Klasa postaci Warrior, dziedzicząca parametry i metody po klasie abstrakcyjnej Character
    /// </summary>
    public class Warrior : Character
    {
        /// <summary>
        /// Konstruktor wywolujacy konstuktor klasu akstrakcyjnej, ustawiajacej podstawowe parametyr postaci
        /// oraz ustawiajacy parametry charakterystyczne dla tej klasy postaci
        /// </summary>
        public Warrior() : base()
        {
            Specialization = "Warrior";
            MaxHealth = Health = 200;
            MinimalAttack = 5;
            MaximumAttack = 10;
            ChanceDefense = 5;
        }
    }
}
